jQuery(document).ready(function( $ ) {
	
    var $form = $('#nbm-calculator');
    
    $form.on('submit', function(e){ 
        
        e.preventDefault();
        
        if( $form[0].checkValidity() ) {


            // Get form data
			var formData = $form.serialize();
            
            
            // ---------------- //
            // More form validity checks here
            // ---------------- //
            
            // If form ok
            
            // Empty response area
            
            $("#nbm-calculator-response").hide().html('');
            
            
            // Show ajax spinner
            // ---------------- //
        
        
            // Set data var ready
            var data = {
                action: 'foobar', // data posted to wp
                data: formData,
                nonce: nbmCalculator.nonce
            };
            
            
            $.post(nbmCalculator.ajaxurl, data, function(response) {
            
                // Hide ajax spinner
                // ---------------- //
                
            
                // Show response
                $("#nbm-calculator-response").html( response ).fadeIn();
                
                // Change text on form submit input
                $("#um-submit").prop("value", "Recalculate");
                
            });
            
            
           

           
            
            
        }
        
    });
	
});