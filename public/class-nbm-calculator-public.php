<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://powerhut.net
 * @since      1.0.0
 *
 * @package    NBM_Calculator
 * @subpackage NBM_Calculator/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    NBM_Calculator
 * @subpackage NBM_Calculator/public
 * @author     Graham Washbrook <graham@powerhut.co.uk>
 */
class NBM_Calculator_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function register_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in NBM_Calculator_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The NBM_Calculator_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_register_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/nbm-calculator-public.css', array(), $this->version, 'all' );

	}



	/**
	 * Enqueue the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in NBM_Calculator_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The NBM_Calculator_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/nbm-calculator-public.css', array(), $this->version, 'all' );
        
        // wp_enqueue_style ( $this->plugin_name );

	}

    /**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function register_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in NBM_Calculator_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The NBM_Calculator_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_register_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/nbm-calculator-public.js', array( 'jquery' ), $this->version, false );

	}


	/**
	 * Enqueue the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in NBM_Calculator_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The NBM_Calculator_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/nbm-calculator-public.js', array( 'jquery' ), $this->version, false );

		// wp_enqueue_script( $this->plugin_name);

	}


    /**
     * Register NBM Calculator Shortcode for the public-facing side of the site.
     *
     * @since   1.0.0
     */
    public function nbm_calculator_shortcode( $atts ){ 
        
        
        // Return if current user is not logged in - Currently disabled
        if ( ! is_user_logged_in() ) {
            // return '<p>Debug: User is not logged in. Do nothing.</p>';
            // return;
        }
        
        // Assign current WP_User object to $current_user var
        // $current_user = wp_get_current_user();
        
        /*
        // Safe usage: Return if current user does not exist - Currently disabled
        if ( ! $current_user->exists() ) {
            // return '<p>Debug: User is logged in, but does not exist. Do nothing.</p>';
        }
        */

        // Localise shortcode-specific previously registered script
        wp_localize_script( 
            $this->plugin_name, // Handle
            'nbmCalculator', // objectName - underscores or camelCase
            array( //Data
                'ajaxurl'     => admin_url( 'admin-ajax.php' ),
                'nonce'   => wp_create_nonce( 'nbm_calculator_nonce' ) 
            ) 
        );


        // Enqueue shortcode-specific previously registered script
        wp_enqueue_script( $this->plugin_name );

        // Enqueue shortcode-specific previously registered style
        wp_enqueue_style ( $this->plugin_name );
    
        // Get array of relevant current user meta 
        // $nbm_calc_data_arr = get_user_meta( $current_user->ID, $key = 'nbm_calc_data', false ); // array
        
        /*
        // Check for array of relevant current user data
        if ( empty ($nbm_calc_data_arr ) ) {

        } else {

        }
        */
        
        // Display data in form
        
        // begin output buffering
        ob_start();
        ?>
    
        <div class="nbm-calculator-wrapper">
    
        <form id="nbm-calculator" class="nbm-calculator">
        
            <fieldset>
            
            <!--<legend>TDEEs calculator</legend>-->
  
            <div class="nbm-form-row">
                <label for="um_goal">Your goal</label>
                <div class="nbm-form-input-container">
                    <select name="um_goal" id="um_goal" required>
                        <option value="" disabled <?php selected( !isset($goal) ); ?>>Please select</option>
                        <option value="1" <?php selected( $goal, '1' ); ?>>Maintain weight</option>
                        <option value="2" <?php selected( $goal, '2' ); ?>>Lose body fat</option>
                        <option value="3" <?php selected( $goal, '3' ); ?>>Gain weight</option>
                    </select>
                </div>
            </div>
  
            <div class="nbm-form-row">
                <label for="um_gender">Gender</label>
                <div class="nbm-form-input-container">
                    <select name="um_gender" id="um_gender" required>
                        <option value="" disabled <?php selected( !isset($gender) ); ?>>Please select</option>
                        <option value="m" <?php selected( $gender, 'm' ); ?>>Male</option>
                        <option value="f" <?php selected( $gender, 'f' ); ?>>Female</option>
                    </select>
                </div>
            </div>

            <div class="nbm-form-row">
                <label for="um_age_range">Age</label>
                <div class="nbm-form-input-container">
                    <select name="um_age_range" id="um_age_range" required>
                        <option value="" disabled <?php selected( !isset($age_range) ); ?>>Please select</option>
                        <option value="1" <?php selected( $age_range, 1 ); ?>>10 - 17</option>
                        <option value="2" <?php selected( $age_range, 2 ); ?>>18 - 29</option>
                        <option value="3" <?php selected( $age_range, 3 ); ?>>30 - 59</option>
                        <option value="4" <?php selected( $age_range, 4 ); ?>>60 - 74</option>
                    </select>
                </div>                
            </div>
  
            <div class="nbm-form-row nbm-form-row-has-2">
                <span>
                    <label for="um_weight">Weight (kg)</label>
                    <input type="number" name="um_weight" id="um_weight" step="0.1" min="10" max="650" required value="<?php if( isset( $weight ) ){ echo $weight;} ?>">
                </span>
                <span>
                    <label for="um_height">Height (cm)</label>
                    <input type="number" name="um_height" id="um_height" step="1" min="50" max="250" required value="<?php if( isset( $height ) ){ echo $height;} ?>">
                </span>
            </div>
  
  
            <div class="nbm-form-row">
                <label for="um_oal">Occupational activity level</label>
                <div class="nbm-form-input-container">
                    <select name="um_oal" id="um_oal" required>
                        <option value="" disabled <?php selected( !isset($oal) ); ?>>Please select</option>
                        <option value="1" <?php selected( $oal, 1 ); ?>>Light</option>
                        <option value="2" <?php selected( $oal, 2 ); ?>>Moderate</option>
                        <option value="3" <?php selected( $oal, 3 ); ?>>Heavy</option>
                    </select>
                </div>
                <div class="nbm-form-help-text">
                    <strong>How active are you at work?</strong> Light would be a sedentary desk job. Moderate might be teaching, hairdressing, on your feet all day. Heavy is building, warehouse-type work, heavy lifting etc.
                </div>
            </div>
        
        
            <div class="nbm-form-row">
                <label for="um_noal">Non-Occupational activity level</label>
                <div class="nbm-form-input-container">
                    <select name="um_noal" id="um_noal" required>
                        <option value="" disabled <?php selected( !isset($noal) ); ?>>Please select</option>
                        <option value="1" <?php selected( $noal, 1 ); ?>>Light</option>
                        <option value="2" <?php selected( $noal, 2 ); ?>>Moderate</option>
                        <option value="3" <?php selected( $noal, 3 ); ?>>Heavy</option>
                    </select>
                </div>
                <div class="nbm-form-help-text">
                    <strong>How active are you outside of work?</strong> Light is up to 3 days a week light exercise. Moderate is 2-5 moderate/hard exercise a week. Heavy is 6-7 days a week hard exercise.
                </div>
            </div>
          
            <div class="nbm-form-row">
                <input id="um-submit" type="submit" value="Calculate" name="um_submit"/>
            </div>
        
            </fieldset>
            
        </form>
        
        </div><!--.nbm-calculator-wrapper-->
        
        <div id="nbm-calculator-response"></div>
        
        <?php
        // end output buffering, assign the buffer contents to html variable, and empty the buffer
        $html = ob_get_clean();
        // trim, compact and return html variable
        return trim( preg_replace('#>\s+<#s', '><', $html) );

    }
    
    
    /**
     * What does this do for the public-facing side of the site.
     *
     * @since   1.0.0
     */
     public function nbm_calculator_handler(){
     
         
        // check_ajax_referer( nbmCalculator.nonce, 'nonce' );
     
        parse_str($_POST['data'], $data);
        
        $goal = $data['um_goal']; // Expect 1|2
        
        $gender = trim( $data['um_gender'] ); // Expect m or f
        $age_range = $data['um_age_range']; // Expect 1|2|3|4
        
        $weight = $data['um_weight']; // expect 10 - 
        
        $height = $data['um_height']; // expect integer 50 - 250
        
        $oal = $data['um_oal']; // Expect 1|2|3
        $noal = $data['um_noal']; // Expect 1|2|3
        
        
        // SCHOFIELD EQUATION: BMR = round( ( $x * weight) + $y ) ) . Note: $x and $y are gender and age dependent
     
        if ( 'm' == $gender ) {
        
                switch ($age_range):
            
                case 1:
                    $x = 17.7;
                    $y = 657;
                break;
                
                case 2:
                    $x = 15.1;
                    $y = 692;
                break;
                
                case 3:
                    $x = 11.5;
                    $y = 873;
                break;
                
                case 4:
                    $x = 11.9;
                    $y = 700;
                break;
                
                default:
                    // not a valid age range, do stuff
                    
            endswitch;
        
        } elseif ( 'f' == $gender) {

            switch ($age_range):
            
                case 1:
                    $x = 13.4;
                    $y = 692;
                break;
                
                case 2:
                    $x = 14.8;
                    $y = 487;
                break;
                
                case 3:
                    $x = 8.3;
                    $y = 846;
                break;
                
                case 4:
                    $x = 9.2;
                    $y = 687;
                break;
                
                default:
                    // not a valid age range, do stuff
                    
            endswitch;
        
        } else {
            // neither m or f, do stuff
        }
     
     
        $bmr = round( ( $x * $weight ) + $y );
     

        // TDEE (Total daily energy expenditure)
        
        // Get PAL factor from array
        
        $pal_arr = array(

            'm' => array (

                '1' => array (

                    '1' => 1.4,
                    '2' => 1.6,
                    '3' => 1.7
                ),

                '2' => array (

                    '1' => 1.5,
                    '2' => 1.7,
                    '3' => 1.8
                ),

                '3' => array (

                    '1' => 1.6,
                    '2' => 1.8,
                    '3' => 1.9
                ),

             ),

            'f' => array (

                '1' => array (
                    '1' => 1.4,
                    '2' => 1.5,
                    '3' => 1.5
                ),

                '2' => array (
                    '1' => 1.5,
                    '2' => 1.6,
                    '3' => 1.6
                ),

                '3' => array (
                    '1' => 1.6,
                    '2' => 1.7,
                    '3' => 1.7
                )

             ),

        );

        
        $pal = $pal_arr[$gender][$noal][$oal];

        $tdee = round( $bmr * $pal );

        
        // Adjust tdee based on goal
        switch ($goal):
            
            case 1: // Maintain weight
                // do nothing
                $calorie_target = $tdee;
            break;
            
            case 2: // Lose body fat
                $calorie_target = $tdee - 500;
            break;
            
            case 3: // Gain weight
                $calorie_target = $tdee + 500;
            
            default:
                // not a valid goal, do stuff
            
        endswitch;

            
        // Calculate protein (g) : weight (kg) x 1.5
        $protein = ceil( $weight * 1.5 );
        
        // If everything stacks up, update user meta
        
        // Display
        echo  '
        <h2 class="response-title">Results</h2>
        <span class="bmr">BMR: ' . $bmr . ' kcal</span>
        <span class="tdee">TDEE: ' . $tdee . ' kcal</span>
        <span class="calorie-target">Calorie target: ' . $calorie_target . ' kcal</span>
        <span class="protein-target">Protein target: ' . $protein . ' g</span>';

        wp_die();
        
     }

}
