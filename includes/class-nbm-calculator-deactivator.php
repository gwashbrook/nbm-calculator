<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://powerhut.net
 * @since      1.0.0
 *
 * @package    NBM_Calculator
 * @subpackage NBM_Calculator/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    NBM_Calculator
 * @subpackage NBM_Calculator/includes
 * @author     Graham Washbrook <graham@powerhut.co.uk>
 */
class NBM_Calculator_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
