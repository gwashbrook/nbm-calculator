<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://powerhut.net
 * @since             1.0.0
 * @package           NBM_Calculator
 *
 * @wordpress-plugin
 * Plugin Name:       NBM Calculator
 * Plugin URI:        https://powerhut.net/nbm-calculator-uri/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.10
 * Author:            Graham Washbrook - Powerhut Net
 * Author URI:        https://powerhut.net/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nbm-calculator
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'NBM_CALCULATOR_VERSION', '1.0.10' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-nbm-calculator-activator.php
 */
function activate_nbm_calculator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nbm-calculator-activator.php';
	NBM_Calculator_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-nbm-calculator-deactivator.php
 */
function deactivate_nbm_calculator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nbm-calculator-deactivator.php';
	NBM_Calculator_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_nbm_calculator' );
register_deactivation_hook( __FILE__, 'deactivate_nbm_calculator' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-nbm-calculator.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_nbm_calculator() {

	$plugin = new NBM_Calculator();
	$plugin->run();

}
run_nbm_calculator();
